from flask import Flask
from flask_slack import Slack

import json

import api.spells
import api.conditions

app = Flask(__name__)
app.config.from_envvar('DNDAPI_SETTINGS')

slack = Slack(app)
app.add_url_rule('/api', view_func=slack.dispatch)


@slack.command('spells', token=app.config['SLACK_TOKEN_SPELLS'], team_id=app.config['SLACK_TEAM_ID'], methods=['POST'])
def spell_search(**kwargs):
    name = kwargs.get('text', '')

    if name.strip() == '':
        return slack.response("No spell name entered")

    result = spells.search(name)

    if len(result) == 0:
        return slack.response("No spells found matching _{}_".format(name))
    elif len(result) > 1:
        error = "Ambiguous result. Please be more specific.\n"
        for row in result:
            error += row['name'] + "\n"

        return slack.response(error)
    else:
        return slack.response(text=spells.format(result[0]), response_type='in_channel')

@slack.command('conditions', token=app.config['SLACK_TOKEN_CONDITIONS'], team_id=app.config['SLACK_TEAM_ID'], methods=['POST'])
def condition_search(**kwargs):
    name = kwargs.get('text', '')

    if name.strip() == '':
        return slack.response("No condition name entered")

    result = conditions.search(name)
    
    if len(result) == 0:
        return slack.response("No spells found matching _{}_".format(name))
    elif len(result) > 1:
        error = "Ambiguous result. Please be more specific.\n"
        for row in result:
            error += row['name'] + "\n"

        return slack.response(error)
    else:
        return slack.response(text=conditions.format(result[0]), response_type='in_channel')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)


