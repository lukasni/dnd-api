import os

import json

with open(os.path.dirname(os.path.realpath(__file__))+'/data/conditions.json') as data_file:
    condition_list = json.load(data_file)

def search(name):
    result = [item for item in condition_list['compendium']['condition'] if item['name'].startswith(name)]

    return result


def format(condition):
    text = ""
    text += "*{name}*\n".format(**condition)
    
    for line in condition['text']:
        text += line + "\n"

    return text


