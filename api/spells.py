import os

import json

spell_schools = {
            'A': 'Abjuration',
            'C': 'Conjuration',
            'D': 'Divination',
            'EN': 'Enchantment',
            'EV': 'Evocation',
            'I': 'Illusion',
            'N': 'Necromancy',
            'T': 'Transmutation'
}

spell_levels = {
            '0': 'Cantrip',
            '1': '1st-level',
            '2': '2nd-level',
            '3': '3rd-level',
            '4': '4th-level',
            '5': '5th-level',
            '6': '6th-level',
            '7': '7th-level',
            '8': '8th-level',
            '9': '9th-level'
}

with open(os.path.dirname(os.path.realpath(__file__))+'/data/spells.json') as data_file:
    spell_list = json.load(data_file)

def search(name):
    result = [item for item in spell_list['compendium']['spell'] if item ['name'].startswith(name)]

    return result

def format(spell):
    text = ""
    text += "*{name}*\n".format(**spell)
    if spell['level'] == '0':
        text += '_' + spell_schools.get(spell['school'], 'Unknown School') + ' Cantrip_\n'
    else:
        text += '_' + spell_levels[spell['level']] + ' ' + spell_schools.get(spell['school'], 'Unknown School') + '_\n'
    text += "*Casting Time:* {time}\n".format(**spell)
    text += "*Duration:* {duration}\n".format(**spell)
    text += "*Components:* {components}\n".format(**spell)
    text += "*Range:* {range}\n\n".format(**spell)

    for line in spell['text']:
        text += line + "\n"
    
    return text


